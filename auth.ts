import NextAuth from 'next-auth';
import { authConfig } from '@/auth.config';
import Credentials from 'next-auth/providers/credentials';
import { z } from 'zod';
import { sql } from '@vercel/postgres';
import type { User } from '@/app/lib/definitions';
import bcrypt from 'bcrypt';

async function getUser(email: string): Promise<User | undefined> {
    try {
        const user = await sql<User>`SELECT * FROM users WHERE email=${email}`;
        console.log('USER');
        console.log(user.rows[0]);
        return user.rows[0];
    } catch (error) {
        return undefined;
        //console.error('Failed to fetch user:', error);
        //throw new Error('Failed to fetch user.');
    }
}

export const { handlers, signIn, signOut, auth  } = NextAuth({
    ...authConfig,
    providers: [
        Credentials({
            credentials: {
                email: { label: "email" },
                password: { label: "password", type: "password" },
                callbackUrl: { label: "callbackUrl", defaultValue: '/dashboard' },
            },
            authorize: async (credentials) => {
                try {
                    let user = null;
                    console.log('AUTH');
                    console.log(auth);
                    console.log('-----------------------------------');
                    console.log('CREDENTIALS');
                    console.log(credentials);
                    //const { email, password, callbackUrl = '/dashboard' } = credentials;
                    const parsedCredentials = z
                        .object({
                            email: z.string().email(),
                            password: z.string().min(6),
                            callbackUrl: z.string().optional().default('/dashboard'),
                            //callbackUrl:     'http://localhost:3000/dashboard'
                        }).safeParse(credentials);
                    //parsedCredentials.data.
                    console.log('PARSE CREDENTIALS');
                    console.log(parsedCredentials);

                    if (parsedCredentials.success) {
                        const { email, password, callbackUrl } = parsedCredentials.data;
                        const user = await getUser(email);
                        if (!user) return null;
                        const passwordsMatch = await bcrypt.compare(password, user.password);

                        if (passwordsMatch) {
                            console.log('pasword match');
                            console.log('AUTH auth.ts');
                            console.log(auth);
                            console.log('USER auth.ts');
                            user.callbackUrl = callbackUrl;
                            console.log(user);

                            return user;
                        } else {
                            return null;
                            //throw new Error('Invalid credentials');
                        };
                    } else {
                        // Validation failed
                        return null;
                        //throw new Error('Invalid credentials'); // Or a more specific error
                    }
                } catch (error) {
                    console.log(error);
                    return null;                 
                }
            },
        }),
    ],
});